# azrs-tracking

Projekat iz predmeta `Alati za razvoj softvera` na Matematičkom fakultetu u Beogradu koji demonstrira primenu alata na projektima [TrafficExpress](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/01-traffic-express) i [AztecTemple](https://gitlab.com/milica-vucic/computer_graphics_2022). 

## Spisak alata :hammer:
- Git
- CMake
- GCov
- Git hooks
- Clang Format
- Clang Analyzer
- Valgrind
- Doxygen
- Docker 
- Clang Tidy

Izveštaji o primeni svih alata su dostupni u okviru sekcije [Issues](https://gitlab.com/milica-vucic/azrs-tracking/-/issues/?sort=created_date&state=all&first_page_size=20).